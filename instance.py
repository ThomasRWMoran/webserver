#!/usr/bin/env python3
import boto3
import time
import urllib.request

def prov():
    provfile = open('nginxProvision','r')
    provdata = provfile.read()
    provfile.close()
    htmlfile = open('nginx.html','r')
    provhtml = htmlfile.read()
    htmlfile.close()
    dict = {"html": provhtml[:provhtml.rfind('\n')]}
    string = provdata.format(**dict)
    return string

def create(tagname):
    ec2 = boto3.resource('ec2',region_name='eu-west-2')

    string = prov()

    myinstance = ec2.create_instances(
        ImageId ='ami-0eb89db7593b5d434',
        InstanceType ='t2.micro',
        SubnetId= 'subnet-01621ca460c3fdf66',
        SecurityGroupIds = ['sg-04df915d2d79b0305'],
        KeyName = 'jenkinsKey',
        MinCount = 1,
        MaxCount = 1,
        UserData = string,
    )

    instIds = []
    for instance in myinstance:
        print("Created instance with ID: "+instance.id)
        tag = ec2.create_tags(Resources=[instance.id], Tags=[
            {'Key':'Name', 'Value':tagname},
            {'Key':'Owner', 'Value':'Tom Moran'},
        ])
        print("Booting up")
        instance.reload()
        while instance.state['Code'] == 0:
            print(".")
            time.sleep(5)
            instance.reload()

        print("\nAt Public IP: "+str(instance.public_ip_address))
        instIds.append(instance.id)
    return instIds

def webAndImage(instanceID, imagename):
    ec2 = boto3.resource('ec2',region_name='eu-west-2')
    ec2client = boto3.client('ec2','eu-west-2')
    response = ec2client.describe_images(
        Filters=[
            {
                'Name': 'name',
                'Values': [
                    imagename,
                ]
            },
        ],
    )
    if response['Images'][0]['ImageId']:
        imid = response['Images'][0]['ImageId']
        img = ec2.Image(imid)
        img.deregister()
    instance = ec2.Instance(instanceID)
    url = 'http://'+str(instance.public_ip_address)
    urlcode = urllib.request.urlopen(url).getcode()
    if urlcode == 200:
        image = instance.create_image(Name=imagename)
        print('Created image '+imagename+'. Id: '+str(image.image_id))
        return str(image.image_id)
    time.sleep(30)
    instance = ec2.Instance(instanceID)
    response = instance.terminate()
    print(response)

def getPrivIP(instanceID):
    ec2 = boto3.resource('ec2',region_name='eu-west-2')
    instance = ec2.Instance(instanceID)
    return instance.private_ip_address

def giveMe5Sec(int=1):
    print("\nGive me "+str(int*5)+"secs")
    for i in range(0, int):
        print(".")
        time.sleep(5)
    print("\n")

#nginxVM = instance.createWprov('tomsFirewall', 'thomasMoran2', 'tomsNginx', prov.nginxProv())
#giveMe5Sec(4)
